import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
  paperWrapper: {
    marginTop: 12,
    width: '100%',
    padding: 12
  }
}))